package ut.com.atlassian.bamboo.plugin.graphiteconnector;

import com.atlassian.bamboo.plugin.graphiteconnector.DefaultGraphiteConnector;
import com.atlassian.bamboo.plugin.graphiteconnector.GraphiteConnector;
import com.atlassian.bamboo.plugin.graphiteconnector.GraphiteConnectorBulkSender;
import com.atlassian.bamboo.plugin.graphiteconnector.GraphiteConnectorConfiguration;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.Socket;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created with IntelliJ IDEA.
 * User: dkovacs
 * Date: 20/06/13
 * Time: 5:24 PM
 */

@RunWith(MockitoJUnitRunner.class)
public class GraphiteConnectorTest {

    @Mock
    private Socket socket;

    @Mock
    private GraphiteConnectorConfiguration configuration;

    private ByteArrayOutputStream resultOutputStream;


    @Before
    public void setup() throws IOException
    {
        this.resultOutputStream = new ByteArrayOutputStream();
        when(socket.getOutputStream()).thenReturn(resultOutputStream);
        when(configuration.getGraphiteSendingEnabled()).thenReturn(true);
        when(configuration.getGraphiteSendingDisabled()).thenReturn(false);
        when(configuration.getSocket()).thenReturn(socket);
        when(configuration.getSourceHostname()).thenReturn("MyHost");
        when(configuration.getPathPattern()).thenReturn("%s.%s.%s");
    }

    @Test
    public void testIfTheCorrectConfigurationIsReturned()
    {
        final GraphiteConnector graphiteConnector = new DefaultGraphiteConnector(configuration);

        assertSame( "Not the same configuration object returned", graphiteConnector.getConfiguration(), configuration );
    }

    @Test
    public void testSendingOneMessage() throws Exception
    {
        final GraphiteConnector graphiteConnector = new DefaultGraphiteConnector(configuration);

        graphiteConnector.write( "MyChannel.MySubChannel", "MyLabel", "1000", 1500 );

        final String expectedData = "MyHost.MyChannel.MySubChannel.MyLabel 1000 1500\n";
        final String writtenData = resultOutputStream.toString();

        assertEquals( "Written data does not match expected data.", expectedData, writtenData );
    }

    @Test
    public void testSendingTwoMessages() throws Exception
    {
        final GraphiteConnector graphiteConnector = new DefaultGraphiteConnector(configuration);

        graphiteConnector.write( "MyChannel.MySubChannel", "MyLabel", "1000", 1500 );
        graphiteConnector.write( "MyChannel2.MySubChannel2", "MyLabel2", "2000", 2500 );

        final String expectedData = "MyHost.MyChannel.MySubChannel.MyLabel 1000 1500\n" +
                "MyHost.MyChannel2.MySubChannel2.MyLabel2 2000 2500\n";
        final String writtenData = resultOutputStream.toString();

        assertEquals( "Written data does not match expected data.", expectedData, writtenData );
    }

    @Test
    public void testIfBulkSenderIsReturned() throws Exception
    {
        final GraphiteConnector graphiteConnector = new DefaultGraphiteConnector(configuration);
        final GraphiteConnectorBulkSender sender = graphiteConnector.getBulkSender();

        assertNotNull( "No object instance is returned by getBulkSender()", sender );
    }

    @Test
    public void testSendingOneMessageWithBulkSender() throws Exception
    {
        final GraphiteConnector graphiteConnector = new DefaultGraphiteConnector(configuration);
        final GraphiteConnectorBulkSender sender = graphiteConnector.getBulkSender();

        sender.write( "MyChannel.MySubChannel", "MyLabel", "1000", 1500 );

        assertEquals( "Unexpected writing: Data was not expected to be written until send() is called.", "", resultOutputStream.toString() );

        sender.send();

        final String expectedData = "MyHost.MyChannel.MySubChannel.MyLabel 1000 1500\n";
        final String writtenData = resultOutputStream.toString();

        assertEquals( "Written data does not match expected data.", expectedData, writtenData );
    }


    @Test
    public void testSendingTwoMessagesWithBulkSender() throws Exception
    {
        final GraphiteConnector graphiteConnector = new DefaultGraphiteConnector(configuration);
        final GraphiteConnectorBulkSender sender = graphiteConnector.getBulkSender();

        sender.write( "MyChannel.MySubChannel", "MyLabel", "1000", 1500 );
        sender.write( "MyChannel2.MySubChannel2", "MyLabel2", "2000", 2500 );

        assertEquals( "Unexpected writing: Data was not expected to be written until send() is called.", "", resultOutputStream.toString() );

        sender.send();

        final String expectedData = "MyHost.MyChannel.MySubChannel.MyLabel 1000 1500\n" +
                "MyHost.MyChannel2.MySubChannel2.MyLabel2 2000 2500\n";
        final String writtenData = resultOutputStream.toString();

        assertEquals( "Written data does not match expected data.", expectedData, writtenData );
    }


    @Test
    public void testSendingThreeMessagesWithBulkSender() throws Exception
    {
        final GraphiteConnector graphiteConnector = new DefaultGraphiteConnector(configuration);
        final GraphiteConnectorBulkSender sender = graphiteConnector.getBulkSender();

        sender.write( "MyChannel.MySubChannel", "MyLabel", "1000", 1500 );
        sender.write( "MyChannel2.MySubChannel2", "MyLabel2", "2000", 2500 );
        sender.write( "MyChannel3.MySubChannel3", "MyLabel3", "3000", 3500 );

        assertEquals( "Unexpected writing: Data was not expected to be written until send() is called.", "", resultOutputStream.toString() );

        sender.send();

        final String expectedData = "MyHost.MyChannel.MySubChannel.MyLabel 1000 1500\n" +
                "MyHost.MyChannel2.MySubChannel2.MyLabel2 2000 2500\n" +
                "MyHost.MyChannel3.MySubChannel3.MyLabel3 3000 3500\n";
        final String writtenData = resultOutputStream.toString();

        assertEquals( "Written data does not match expected data.", expectedData, writtenData );
    }

    @Test
    public void testIfCorrectSentMessageCountIsReturned()
    {
        final GraphiteConnector graphiteConnector = new DefaultGraphiteConnector(configuration);
        final GraphiteConnectorBulkSender sender = graphiteConnector.getBulkSender();

        sender.write( "MyChannel.MySubChannel", "MyLabel", "1000", 1500 );
        sender.write( "MyChannel2.MySubChannel2", "MyLabel2", "2000", 2500 );
        int sentMessageCount = sender.send();

        assertEquals( "Sent message count and sent message count does not match", 2, sentMessageCount );
    }

    @Test
    public void testIfMessageQueueCleanedAfterSending()
    {
        final GraphiteConnector graphiteConnector = new DefaultGraphiteConnector(configuration);
        final GraphiteConnectorBulkSender sender = graphiteConnector.getBulkSender();

        sender.write( "MyChannel.MySubChannel", "MyLabel", "1000", 1500 );
        sender.write( "MyChannel2.MySubChannel2", "MyLabel2", "2000", 2500 );
        sender.send();
        resultOutputStream.reset();

        // This should send nothing!
        sender.send();

        final String writtenData = resultOutputStream.toString();
        assertEquals( "Unexpected writing: Writing after send() detected.", "", writtenData );
    }





}
