package com.atlassian.bamboo.plugin.graphiteconnector;

import com.atlassian.bamboo.configuration.AdministrationConfigurationAccessor;
import com.atlassian.bamboo.configuration.GlobalAdminAction;
import com.atlassian.bamboo.security.BambooPermissionManager;
import com.atlassian.bamboo.user.BambooAuthenticationContext;
import com.atlassian.bamboo.ww2.BambooActionSupport;
import com.atlassian.bandana.BandanaManager;
import com.atlassian.user.User;
import org.jetbrains.annotations.NotNull;;
import org.apache.commons.lang.StringUtils;

import java.io.IOException;
import java.net.Socket;

/**
 * Implementation of GraphiteConnectorConfiguration
 *
 * This class is used as a configuration data for GraphiteConnector, and as a configuration editing web resource as well.
 *
 */
public class DefaultGraphiteConnectorConfiguration extends GlobalAdminAction implements GraphiteConnectorConfiguration {

    private final BambooAuthenticationContext bambooAuthenticationContext;
    private final BambooPermissionManager bambooPermissionManager;
    private final BandanaManager bandanaManager;

    private final BandanaValue<Boolean> bandanaGraphiteSendingEnabled;
    private final BandanaValue<String> bandanaGraphiteHostName;
    private final BandanaValue<Integer> bandanaGraphitePort;

    //private volatile Boolean graphiteSendingEnabled;
    private volatile Boolean graphiteSendingDisabled;
    // Yeah, this is stupid but necessary since our setGraphiteSendingEnabled() method was only called when the checkbox was checked.
    // If the checkbox was not checked, no calls were made to setGraphiteSendingEnabled(), so it was impossible to switch sending off.
    // The workaround is to introduce a new variable, which is always False, unless it is set to True before doExecute.

    private volatile String sourceHostName;

    public DefaultGraphiteConnectorConfiguration( @NotNull AdministrationConfigurationAccessor administrationConfigurationManager,
                                                  @NotNull BambooAuthenticationContext bambooAuthenticationContext,
                                                  @NotNull BambooPermissionManager bambooPermissionManager,
                                                  @NotNull BandanaManager bandanaManager ) {
        this.bambooAuthenticationContext = bambooAuthenticationContext;
        this.bambooPermissionManager = bambooPermissionManager;
        this.bandanaManager = bandanaManager;
        sourceHostName = GraphiteConnectorConstants.sanitizeGraphiteHostName(administrationConfigurationManager.getAdministrationConfiguration().getBaseUrl());
        bandanaGraphiteSendingEnabled = new BandanaValue <Boolean> ( bandanaManager, GraphiteConnectorConstants.BANDANA_KEY_GRAPHITE_SENDING_ENABLED, GraphiteConnectorConstants.DEFAULT_GRAPHITE_SENDING_ENABLED );
        bandanaGraphiteHostName = new BandanaValue <String> ( bandanaManager, GraphiteConnectorConstants.BANDANA_KEY_GRAPHITE_HOST_NAME, GraphiteConnectorConstants.DEFAULT_GRAPHITE_HOST_NAME );
        bandanaGraphitePort = new BandanaValue <Integer> ( bandanaManager, GraphiteConnectorConstants.BANDANA_KEY_GRAPHITE_PORT, GraphiteConnectorConstants.DEFAULT_GRAPHITE_PORT );
        graphiteSendingDisabled = false;
    }

    @Override
    public String doExecute() throws Exception {
        final User user = bambooAuthenticationContext.getUser();
        if (user != null && bambooPermissionManager.isAdmin(user.getName())) {
            return super.doExecute();
        }
        return ERROR;
    }

    public String doEdit() {
        bandanaGraphiteSendingEnabled.set( ! this.graphiteSendingDisabled );
        return SUCCESS;
    }

    @Override
    public void setGraphiteSendingDisabled(Boolean graphiteSendingDisabled) {
        this.graphiteSendingDisabled = graphiteSendingDisabled;
    }

    @Override
    public Boolean getGraphiteSendingDisabled() {
        return ! bandanaGraphiteSendingEnabled.get();
    }

    @Override
    public void setGraphiteSendingEnabled(Boolean graphiteSendingEnabled) {
        bandanaGraphiteSendingEnabled.set( graphiteSendingEnabled );
    }

    @Override
    public Boolean getGraphiteSendingEnabled() {
        return bandanaGraphiteSendingEnabled.get();
    }

    @Override
    public void setGraphiteHostName(String hostName) {
        bandanaGraphiteHostName.set( hostName );
    }

    @Override
    public String getGraphiteHostName() {
        return bandanaGraphiteHostName.get();
    }

    @Override
    public void setGraphitePort(Integer port) {
        bandanaGraphitePort.set( port );
    }

    @Override
    public Integer getGraphitePort() {
        return bandanaGraphitePort.get();
    }

    @Override
    public String getPathPattern() {
        return GraphiteConnectorConstants.DEFAULT_PATH_PATTERN;
    }

    @Override
    public String getSourceHostname() {
        return sourceHostName;
    }

    @Override
    public Socket getSocket() throws IOException {
        final Socket socket = new Socket( this.getGraphiteHostName(), this.getGraphitePort() );
        socket.setSoTimeout(GraphiteConnectorConstants.SOCKET_TIMEOUT);
        return socket;
    }
}
