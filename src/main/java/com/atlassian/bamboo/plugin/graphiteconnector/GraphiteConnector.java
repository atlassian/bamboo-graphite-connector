package com.atlassian.bamboo.plugin.graphiteconnector;

/**
 * Graphite Connector Interface
 *
 * Using this interface measurement data can be sent to Graphite Server.
 *
 */
public interface GraphiteConnector extends GraphiteConnectorSender, GraphiteConnectorBulkSenderFactory {

    /**
     * Returns the currently used configuration instance.
     *
     * @return Instance of an object supporting the GraphiteConnectorConfiguration interface.
     */
    public GraphiteConnectorConfiguration getConfiguration();

}